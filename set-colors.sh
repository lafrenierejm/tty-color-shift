#!/bin/env sh

# perror()
## Print to stderr
perror()
{
	printf "%b" "$*" >&2;
}

# Ensure that the colour files exist.
# If either file is missing, exit 1.
## Get the absolute path of this script, following symlinks.
## Then get the absolute path of its containing directory,
##+which should contain the colour files.
## Check that directory for files named "day" and "night".
## If either file is missing, print error message and exit.
script=$(readlink -f "$0")		# Get script's path
scriptPath=$(dirname "$script")	# Get script's directory
if ! [ -f "${scriptPath}/day" ]	# Check for missing day
then
	if ! [ -f "${scriptPath}/night" ]	# Check for also missing night
	then
		perror "$0: Missing day and night color files in ${scriptPath}\n"
		exit 1
	fi
	perror "$0: Missing day color file in ${scriptPath}\n"
	exit 1
fi
if ! [ -f "${scriptPath}/night" ]	# Check for only missing night
then
	perror "$0: Missing night color file in ${scriptPath}\n"
	exit 1
fi

# Set the tty number
tty="/dev/tty2"

# Get the hour (0-23)
hour=$(date +%H)

# Default to day theme
themeFile="${scriptPath}/day"
# Night theme from 18:00-06:00
if [ "${hour}" -lt 6 ] || [ "${hour}" -gt 18 ]
then
	themeFile="${scriptPath}/night"
fi

# Print the colors in $themeFile to $tty
## dash's printf doesn't work, so get the system's printf binary
printf=$(which printf)
## Read each line in $themeFile, storing its value in $colorHex
## Prefix $colorHex with "\e]" and a hexadecimal count,
##+then print to $tty
## Must use printf's aboslute path; dash's printf won't work
colorNum=0
while read -r colorHex;
do
	${printf} "\e]P%x%s" ${colorNum} "${colorHex}" > "${tty}"
	colorNum=$((colorNum + 1))
done < "${themeFile}"

# If tmux is running, refresh the client running in $tty
pgrep tmux > /dev/null && tmux refresh-client -t${tty}
